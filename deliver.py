import requests
import greenstalk
import json
from datetime import datetime, timezone
from email.utils import format_datetime
from urllib.parse import urlparse
from OpenSSL import crypto
from base64 import b64encode
import hashlib

client = greenstalk.Client(('127.0.0.1', 11300),use='ActivityPets',watch='ActivityPets')
# get version for user agent
with open('useragent','r') as file:
	useragent = file.read()
	useragent = useragent.rstrip()

while True:
	
	with open('logs/deliver.log','a') as log:
		try:
			log.write("waiting for jobs\n")
			job = client.reserve()
		except greenstalk.TimedOutError as e:
			# log timeout
			continue
	
		log.write(f"claimed job {job.id}\n")
		message = json.loads(job.body)
		
		# change getting key to db call
		key_path = "keys/"+str(message['user'])
		try:
			key_file = open(key_path, "r")
		except:
			log.write("invalid key file\n")
			client.delete(job)
			continue
		key = key_file.read()
		key_file.close()
		pkey = crypto.load_privatekey(crypto.FILETYPE_PEM, key)
		
		timestmp = format_datetime(datetime.now(timezone.utc), usegmt=True)
		url = message['inbox']
		purl = urlparse(url)
		url_host = purl.netloc
		url_path = purl.path
		hash = hashlib.sha256(message['content'].encode('utf-8'))
		digest = hash.digest()
		digest = b64encode(digest)
		digstring = 'SHA-256='+digest.decode('utf-8')

		to_sign = f"(request-target): post {url_path}\nhost: {url_host}\ndate: {timestmp}\ndigest: {digstring}"
		signature = crypto.sign(pkey, to_sign.encode('utf-8'), "sha256")
		signature = b64encode(signature)
			
		keyid = 'keyId="'+message['actor']+'#main-key"'
		algo = 'algorithm="rsa-sha256"'
		hlist = 'headers="(request-target) host date digest"'
		content_length = str(len(message['content']))
		sigstring = 'signature="'+signature.decode('utf-8')+'"'

		headers = {
								'user-agent': useragent,
								'content-type': 'application/activity+json',
								'accept': 'application/activity+json,application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
								'content-length': content_length,
								'host': url_host,
								'date': timestmp,
								'digest': digstring,
								'signature': ','.join([keyid,algo,hlist,sigstring])
		}
		data = message['content']
		post = json.loads(data)
		log.write(f"sending {post['id']} to {url} from {message['actor']}\n")
		try:
			result = requests.post(url, headers=headers, data=data)
		except Exception as e:
			log.write(e.errno,e.message)
			if (e.errno == -3):
				log.write("putting back in the queue\n")
				try:
					client.release(job,delay=600)
				except greenstalk.NotFoundError:
					log.write("job already gone\n")
					continue

			else:
				log.write("cancelling delivery\n")
				try:
					client.delete(job)
				except greenstalk.NotFoundError:
					log.write("job already gone\n")
					continue
		else:

			# verify status
			log.write(f"request sent - {result.status_code}\n")
			
			# long delay:
			if result.status_code >= 500:
				log.write("putting back in the queue\n")
				try:
					client.release(job,delay=3600)
				except greenstalk.NotFoundError:
					log.write("job already gone\n")
					continue
			#short delay
			elif result.status_code >= 400:
				if result.status_code == 408:
					log.write("putting back in the queue\n")
					try:
						client.release(job,delay=60)
					except greenstalk.NotFoundError:
						log.write("job already gone\n")
						continue

				else:
					# add error log
					log.write("cancelling delivery\n")
					try:
						client.delete(job)
					except greenstalk.NotFoundError:
						log.write("job already gone\n")
						continue
					
			#no delay
	#		client.release(job)

			else:
				try:
					client.delete(job)
				except greenstalk.NotFoundError:
					log.write("job already gone\n")
					continue

client.close()
