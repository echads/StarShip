<?php

function user_agent() {
	return "StarShip v". trim(file_get_contents('version'));
}

function debug( $msg, $level, $verbose = false ) {
	global $debug_level;
	if ($level <= $debug_level) {
		$backtrace = debug_backtrace(1,0);
		if ($verbose) {
			$print = '';
			foreach ($backtrace as $trace) {
				$trace['file'] = str_replace( getcwd() .'/', '', $trace['file']);
				$print .= $trace['file'] .':'. $trace['line'] ."\n";
				$print .= '  '. $trace['function'] .'('. implode(', ',$trace['args']) .")\n";
			}
			$print .= $msg;
		}
		else {
			$backtrace = $backtrace[0];
			$backtrace['file'] = str_replace( getcwd() .'/', '', $backtrace['file']);
			$print = $backtrace['file'] .':'. $backtrace['line'] .' '. $msg;
			file_put_contents('logs/debug.log',$print ."\n",FILE_APPEND);
		}
	}
}

?>