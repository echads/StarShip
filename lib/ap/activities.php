<?php
namespace StarShip {

include_once 'lib/objects.php';

/* The base parent class for all ActivityPub Activities */
class Activity {
  
  // define the elements that other stuff should be able to access
  public $actor;
  public $obj;
  public $address;
  public $type;
  // this is just to track whether or not an object has been initialized with content or is empty
  public $new = true;
  
  // load an activity's properties from the database into the Activity object
  function load($id, &$sql) {
		global $baseurl;
    if ($this->new === false) throw new Exception('requires a blank Activity object');

		$query = $sql->prepare("SELECT * FROM activities WHERE id = ?");
		$query->bind_param('s',$id);
		$query->execute();
		if ($query->errno) {
			echo "SQL error: (" . $query->errno . ") " . $query->error ."\n"; return false;
		}
		$result = $query->get_result();
		if ($row = $result->fetch_assoc()) {
			$this->id = $row['id'];
			
			$this->activity_array = [
					'id'        => $baseurl .'/ap/activity?'. $row['id'],
					'type'      => $row['activity_type'],
					'actor'     => $baseurl .'/ap/actor?'. $row['actor'],
					'published' => date("Y-m-d\TH:i:s\Z"),
					'to'        => json_decode($row['to_addr'])
				];
			if ($row['cc_addr']) $this->activity_array['cc'] = json_decode($row['cc_addr']); else $this->activity_array['cc'] = [];
			if ($row['bcc_addr']) $this->activity_array['bcc'] = json_decode($row['bcc_addr']); else $this->activity_array['bcc'] = [];
			
			if (!is_array($this->activity_array['to'])) $this->activity_array['to'] = array($this->activity_array['to']);
			if (!is_array($this->activity_array['cc'])) $this->activity_array['cc'] = array($this->activity_array['cc']);
			if (!is_array($this->activity_array['bcc'])) $this->activity_array['bcc'] = array($this->activity_array['bcc']);

			if ($row['obj_id']) {
				$this->obj = new APObject();
				$this->obj->load($row['obj_id'],$sql);
				$this->activity_array['object'] = json_decode($this->obj->toJSON());
			}
			else if ($row['obj_str']) {
				$this->obj = $row['obj_str'];
				$this->activity_array['object'] = $row['obj_str'];
			}
			return true;
		}
  }

  // Stores the properties of the Activity object into the database
  function save(&$sql) {
    if ($this->new === true) throw new Exception('requires a populated Activity object');
		
		if (is_string($this->obj)) { $obj_id = null; $obj_str = $this->obj; }
		else if (is_array($this->obj)) { $obj_id = $this->obj['id']; $obj_str = null; }
		else { $obj_id = $this->obj->id(); $obj_str = null; if (!$this->obj->save($sql)) return false; }
		$query = $sql->prepare("INSERT INTO activities (id, actor, activity_type, obj_id, obj_str, created_at, to_addr, cc_addr, bcc_addr ) VALUES (?,?,?,?,?,NOW(),?,?,? )");
		$to_addr = json_encode($this->address['to']); $cc_addr = json_encode($this->address['cc']); $bcc_addr = json_encode($this->address['bcc']);
		$query->bind_param('ssssssss',$this->id, $this->actor, $this->type, $obj_id, $obj_str, $to_addr,$cc_addr,$bcc_addr);
		$query->execute();
		if ($query->errno) {
			echo "SQL error: (" . $query->errno . ") " . $query->error ."\n"; return false;
		}
		else return true;
  }

  /* loads an activity's properties from an unserialized-JSON array into the Activity object */
  function fill($json_array) {
		global $baseurl;
    if ($json_array === null) throw new Exception('no Activity data provided');
    // verify that certain required properties are set
    $valid = self::validateActivity($json_array);
    if ($valid === false) {
			debug(print_r($json_array,true),3);
			throw new Exception('invalid Activity data');
		}

    // loads the array into the object, isn't this nice
    $this->activity_array = $json_array;
    $this->new = false;
    if (!isset($this->activity_array['id'])) {
			$this->id = str_replace('.','',uniqid('',true));
			$this->activity_array['id'] = $baseurl .'/ap/activity?'. $this->id;
		}
		else {
			$this->id = self::clip($this->activity_array['id']);
		}

    // assign public values to appropriate variables

    if (!isset($this->activity_array['object'])) $this->activity_array['object'] = null;
		else if (!is_string($this->activity_array['object'])) {
				try {
					$this->obj = new APObject();
					$this->obj->fill($this->activity_array['object']);
				}
				catch (Exception $e) {
					$this->obj = $this->activity_array['object'];
				}
		}
		else $this->obj = &$this->activity_array['object'];

    $this->actor    = &$this->activity_array['actor'];
    $this->type     = &$this->activity_array['type'];
    $this->address  = [];
    if (isset($this->activity_array['to']))  $this->address['to']  = &$this->activity_array['to'];
		else $this->address['to']  = [];
    if (isset($this->activity_array['cc']))  $this->address['cc']  = &$this->activity_array['cc'];
		else $this->address['cc']  = [];
    if (isset($this->activity_array['bcc'])) $this->address['bcc'] = &$this->activity_array['bcc'];
		else $this->address['bcc']  = [];
  }
	
	public function wrap(&$Object, $type='Activity') {
		global $baseurl;
		$this->obj = &$Object;
		$this->type = $type;
		$this->actor = $this->obj->attributedTo;
		$this->activity_array['object'] = json_decode($this->obj->toJSON());
		$this->id = str_replace('.','',uniqid('',true));		
		$this->activity_array = [
				'type'      => $type,
				'actor'     => $baseurl .'/ap/actor?'. $this->obj->attributedTo,
				'published' => $this->obj->published(),
				'to'        => $this->obj->address['to'],
			];
		$this->activity_array['id'] = $baseurl .'/ap/activity?'. $this->id;
		if (isset($this->obj->address['cc'])) $this->activity_array['cc'] = $this->obj->address['cc']; else $this->activity_array['cc'] = [];
		if (isset($this->obj->address['bcc'])) $this->activity_array['bcc'] = $this->obj->address['bcc']; else $this->activity_array['bcc'] = [];
		$this->address['to'] = &$this->activity_array['to'];
		$this->address['cc'] = &$this->activity_array['cc'];
		$this->address['bcc'] = &$this->activity_array['bcc'];
		$this->new = false;	
	}
  
  // this is a function instead of a variable because changing the id is Very Bad
  public function id($full=false) {
		if ($full) return $this->activity_array['id'];
		else return $this->id;
  }
  
  /* Serializes the Activity object to a ActivityPub JSON-LD object */
  public function toJSON() {
		if (is_array($this->obj) || is_string($this->obj)) $this->activity_array['object'] = $this->obj;
		else $this->activity_array['object'] = json_decode($this->obj->toJSON());

    // this is gonna have to have an actual context string
    $this->activity_array['@context'] = "https://www.w3.org/ns/activitystreams";
    $json = json_encode($this->activity_array);
    unset($this->activity_array['@context']);
    return $json;
  }

  protected $activity_array;
  protected $id;

  /* Check various key properties to make sure that they exist and have valid contents */
  protected function validateActivity(&$json_array) {
    $test_array = [ "Create", "Update", "Delete", "Follow", "Accept", "Reject",
                    "Add", "Remove", "Like", "Block", "Undo" ];
    // make sure it is an accepted ActivityPub activity type
    if (!isset($json_array['type']) || !in_array($json_array['type'],$test_array)) return false;
    // all Activities must have an actor and an id
/*
      I need to figure out a different way of handling the id check, because client-server
      Activities are NOT supposed to have an id on them already and the server is supposed to
      generate one, while server-server Activities are REQUIRED to have an id...
      
      I guess I could have a client vs. server flag in this function which tests
      appropriately, and have the flag passed from the constructor? That way, the constructor
      could generate the unique id if it's from a client.
*/   
    if (!isset($json_array['actor']) /* || !isset($json_array['id']) */) return false;
    // all Activities except for Deletes must have an object
    if ($json_array['type'] !== "Delete" && !isset($json_array['object'])) return false;
    // objects can be array or URI (treated as ID)
		if (isset($json_array['object']) && is_array($json_array['object']) && $json_array['object']['type'] != 'Follow') {
		// all Object arrays must have an ID and a type
      if (!isset($json_array['object']['id']) || !isset($json_array['object']['type'])) return false;
      // verify actor attribution
      if (isset($json_array['object']['attributedTo'])
        && $json_array['object']['attributedTo'] !== $json_array['actor']) return false;
      // verify addressing exists according to spec
      if (isset($json_array['object']['to'])) {
        if (!isset($json_array['to'])) return false;
        if ($json_array['to'] !== $json_array['object']['to']) return false;
      }
      if (isset($json_array['object']['cc'])) {
        if (!isset($json_array['cc'])) return false;
        if ($json_array['cc'] !== $json_array['object']['cc']) return false;
      }
      if (isset($json_array['object']['bcc'])) {
        if (!isset($json_array['bcc'])) return false;
        if ($json_array['bcc'] !== $json_array['object']['bcc']) return false;
      }
    }
    return true;
  }
	
	// exists just to pull the local ID back off the full AP ID
	protected function clip($string) {
		$snip = strpos($string, '?');
		return substr($string, $snip+1);
	}

}

/* class for Create Activities; see AP spec */
class Create extends Activity {
	// i hate this but let's try it
  function wrap(&$Object, $type='Create') {
		parent::wrap($Object, $type='Create');
  }  

}

/* class for Follow Activities; see AP spec. Also handles Accept and Reject activities by local accounts */
class Follow extends Activity {  
  public function accept() {
    // create a new Activity of type 'Accept', with the Follow id as the object, and return it
    $arr = [
              "type"    => "Accept",
              "object"  => $this->activity_array,
              "actor"   => $this->obj
           ];
    $accept = new Activity();
    $accept->fill($arr);
    // post new activity to the outbox
    return $accept; 
  }
  
  public function reject() {
    // create a new Activity of type 'Reject', with the Follow id as the object, and return it
    $arr = [
              "type"    => "Reject",
              "object"  => $this->activity_array,
              "actor"   => $this->obj
           ];
    $reject = new Activity();
    $reject->fill($arr);
    // post new activity to the outbox
    return $reject; 
  }
}

class ProfileUpdate extends Activity {
	public function load($username,&$sql) {
		global $baseurl, $manager;
		require_once 'func/meta.php';

		$this->id = str_replace('.','',uniqid('',true));
		$actor = $baseurl .'/ap/actor?'. $username;

		if ($username == $manager)  $query = $sql->prepare("SELECT * FROM accounts WHERE username = ?");
		else $query = $sql->prepare("SELECT * FROM accounts JOIN characters ON accounts.uuid = characters.uuid WHERE username = ?");
		$query->bind_param('s',$username);
		$query->execute();
		if ($query->errno) {
			file_put_contents("logs/profile.log",$query->errno .': '. $query->error ."\n",FILE_APPEND);
			return false;
		}
		$result = $query->get_result();

		if (!($row = $result->fetch_assoc())) {
			file_put_contents("logs/profile.log","account not found\n",FILE_APPEND);
			return false;
		}
		$atts = [];
		if ($username != $manager) {
			$atts[] = [
							   "type" => "PropertyValue",
							   "name" => "Partner",
							   "value" => $row['partner'],
			];
			$query = $sql->prepare("SELECT name FROM places WHERE id = ?");
			$query->bind_param('i',$row['home']); $query->execute();
			if ($query->errno) {
				// log error locally
				debug($query->errno .': '. $query->error,2);
			}
			else {
				$result = $query->get_result();
				if ($home = $result->fetch_assoc()) {
					$atts[] = [ 'type' => 'PropertyValue', 'name' => 'Home', 'value' => $home['name'] ];
				}
			}
			$quest = get_quest($row['quest_type'], $row['quest_target'], $sql);
			if ($quest) {
				$atts[] = [ "type" => "PropertyValue", "name" => "Current Quest", "value" => $quest ];
			}
		}
		$obj_arr = [
			"id" => $actor,
			"type" => "Service",
			"preferredUsername" => $username,
			"name" => $row['displayname'],
			"icon" => [
				"type" => "Image",
				"mediaType" => "image/png",
				"url" => $baseurl . '/i?'. $username
			],
			"summary" => $row['bio'],
			"inbox" => $baseurl . '/ap/inbox?'. $username,
			"outbox" => $baseurl . '/ap/outbox?'. $username,
			"followers" => $baseurl . '/ap/followers?'. $username,
			"url" => $baseurl . '/profile?'. $username,
			"publicKey" => [
				"id" => $actor .'#main-key',
				"owner" => $actor,
				"publicKeyPem" => $row['pubkey']
			],
			"attachment" => $atts
		];
		$arr = [
						"id"        => $baseurl .'/ap/activity?'. $this->id,
	          "type"      => "Update",
						"object"    => $obj_arr,
						"actor"     => $actor,
						"published" => date("Y-m-d\TH:i:sZ"),
			      'to'        => $baseurl .'/ap/followers?'. $username			
					 ];

		$this->activity_array = $arr;
		$this->obj = &$this->activity_array['object'];
		$this->address = [ 'to' => &$this->activity_array['to'] ];
		return true;
	}
  public function toJSON() {
		if (!is_array($this->obj) && !is_string($this->obj)) {
			$this->activity_array['object'] = json_decode($this->obj->toJSON());
		}
    // this is gonna have to have an actual context string
    $this->activity_array['@context'] = [ "https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1" ];
    $json = json_encode($this->activity_array);
    unset($this->activity_array['@context']);
    return $json;
  }
}

/*
class Update extends Activity {
  
}

class Delete extends Activity {
  
}

class AddRemove extends Activity {
  
}
*/

}?>
