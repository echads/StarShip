<?php
require_once "lib/activities.php";

namespace StarShip {

/*
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "summary": "Sally's notes",
  "type": "Collection",
  "totalItems": 2,
  "items": [
    {
      "type": "Note",
      "name": "A Simple Note"
    },
    {
      "type": "Note",
      "name": "Another Simple Note"
    }
  ]
}
*/
class Collection {
  
  // define the elements that other stuff should be able to access
  public $actor;
  public $obj;
  public $address;
  public $type = 'Collection';
  // this is just to track whether or not an object has been initialized with content or is empty
  public $new = true;
  
  /* loads an activity's properties from an unserialized-JSON array into the Activity object */
  function fill($json_array) {
    if ($json_array === null) throw new Exception('no Activity data provided');

    // verify that certain required properties are set
    $valid = self::validateActivity($json_array);
    if ($valid === false) throw new Exception('invalid Activity data');

    // loads the array into the object, isn't this nice
    $this->activity_array = $json_array;
    $this->new = false;
    if (!isset($this->activity_array['id'])) $this->activity_array['id'] = "https://". $_SERVER['HTTP_HOST'] ."/ap/actor/". uniqid(); // need a proper path later

    // Should probably do something with the @context array to identify unrecognized properties and decide how to handle them
    // but that can wait until later, when the capability for handling a variety of things Actually Exists
    unset($this->activity_array['@context']);
    if (!isset($this->activity_array['object'])) $this->activity_array['object'] = null;

    // assign public values to appropriate variables
    $this->actor    = &$this->activity_array['actor'];
    $this->type     = &$this->activity_array['type'];
    $this->obj      = &$this->activity_array['object'];
    $this->address  = [];
    if (isset($this->activity_array['to']))  $this->address['to']  = &$this->activity_array['to'];
    if (isset($this->activity_array['cc']))  $this->address['cc']  = &$this->activity_array['cc'];
    if (isset($this->activity_array['bcc'])) $this->address['bcc'] = &$this->activity_array['bcc'];
  }
  
  // this is a function instead of a variable because changing the id is Very Bad
  public function id() {
    return $this->activity_array['id'];
  }
  
  // load all activities contained by the Collection and return as an array
  public function fetch_activities() {
    if ($new === true) throw new Exception('requires a populated Activity object');
  }

  // load a subset of activities contained by the Collection defined by the page and return as an array
  public function fetch_activity_page($page) {
    if ($new === true) throw new Exception('requires a populated Activity object');
  }

  /* Serializes the Collection object to a ActivityPub JSON-LD object */
  public function toJSON($page=false) {
		$collection = [];
		$collection['@context'] = "https://www.w3.org/ns/activitystreams";
		$collection['summary'] = $this->get_summary();
		$collection['type'] = $this->type;
		
		if ($page) {
			$activities = $this->fetch_activity_page($page);
		} else {
			$activities = $this->fetch_activities();
		}
		
		$collection['items'] = $activities;
    $json = json_encode($collection);
    return $json;
  }

  protected $id;

}

/*
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "summary": "Sally's notes",
  "type": "OrderedCollection",
  "totalItems": 2,
  "orderedItems": [
    {
      "type": "Note",
      "name": "A Simple Note"
    },
    {
      "type": "Note",
      "name": "Another Simple Note"
    }
  ]
}
*/
class OrderedCollection extends Collection {
  public $type = 'OrderedCollection';

  // load a subset of activities contained by the Collection defined by the start ID and return as an array
  function fetch_activities_from($activityID) {
    if ($new === true) throw new Exception('requires a populated Activity object');
  }
}

}?>
