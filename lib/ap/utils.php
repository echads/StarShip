<?php
use xobotyi\beansclient\BeansClient;
use xobotyi\beansclient\Connection;

function dashedCamelCase($string, $capitalizeFirstCharacter = false) {
  $str = str_replace('-', '-', ucwords($string, '-'));
  if (!$capitalizeFirstCharacter) {
    $str = lcfirst($str);
  }
  return $str;
}

/*
  work-around for having JUST too old a version of PHP to use getallheaders()
*/
function getRequestHeaders() {
  $headers = array();
  foreach($_SERVER as $key => $value) {
    if (substr($key, 0, 5) <> 'HTTP_') {
      continue;
    }
    $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
    $headers[$header] = $value;
  }
  return $headers;
}

/* does what it says on the tin */
/* returns TRUE on success, FALSE on invalid signature, and a specific error string on error */
function verify_signature(&$headers, &$actor, $uid) {
  $sig_array = [];
  $sig_s = '';

  // break apart comma-separated string of id=value items for the signature
  if ($arr = explode(',', $headers["Signature"])) {
    foreach ($arr as $item) {
      if ($pos = strpos($item, '=')) {
        $sig_array[trim(substr($item, 0, $pos))] = trim(substr($item, $pos + 1),'"');
      }
      else $sig_array["nullIDs"] .= $item;
    }
  }

  if (!isset($sig_array["keyId"])) return "no keyId found";

  // should add a check to cross-check actor/owner entries later
  if (($pubkey = get_pubkey($actor,$sig_array["keyId"])) === false) return "public key not found";

  $signed_headers = explode(' ',$sig_array['headers']);
  $signed_data = '';
	$backup_signed_data = '';

  //add a check for encryption algorithm and use the appropriately indicated one

  foreach ($signed_headers as $item) {
    if ($item == '(request-target)') {
			$signed_data .= "(request-target): post /ap/inbox?$uid\n";
			$backup_signed_data .= "(request-target): post /ap/inbox\n";
		}
    else if ($item == 'digest') {
			$encode = base64_encode(hash("sha256",file_get_contents("php://input"),true));
			$signed_data .= "digest: SHA-256=$encode\n";
			$backup_signed_data .= "digest: SHA-256=$encode\n";
		}
    else {
			$signed_data .= "$item: ". $headers[dashedCamelCase($item,true)] ."\n";
			$backup_signed_data .= "$item: ". $headers[dashedCamelCase($item,true)] ."\n";
		}
  }

  $verification = openssl_verify(
    trim($signed_data), //trimmed to avoid trailing newlines
    base64_decode($sig_array['signature']), //decoded signature string
    $pubkey, //use PEM text from actor
    OPENSSL_ALGO_SHA256 //algorithm flag should be set from headers
  );

  if ($verification === 1) return true;
  else if ($verification === 0) {
		$verification2 = openssl_verify(
			trim($backup_signed_data), //trimmed to avoid trailing newlines
			base64_decode($sig_array['signature']), //decoded signature string
			$pubkey, //use PEM text from actor
			OPENSSL_ALGO_SHA256 //algorithm flag should be set from headers
		);
		if ($verification2 === 1) return true;
		else if ($verification2 === 0) return false;
		else return openssl_error_string();
	}
  else if ($verification == -1) return openssl_error_string();
}

/* fetches the public key for a remote actor to verify their signed data */
function get_pubkey(&$actor, $keyId) {
  $key_uri = $keyId;
	
  // make sure there IS a public key
  if (isset($actor['publicKey'])) {
    // check that the public key says it's the same key we're looking for
    if (!isset($actor['publicKey']['id']) || $actor['publicKey']['id'] !== $keyId) return false;
    // return the public key PEM, if there is one
    if (isset($actor['publicKey']['publicKeyPem'])) return $actor['publicKey']['publicKeyPem'];
    else return false;
  }
  else return false;
}

function get_actor($actorID,$uuid,$username) {
	global $baseurl;
  $date = date("D, d M Y H:i:s") . " GMT";
  // header information to sign
  $data = "(request-target): get ". parse_url($actorID,PHP_URL_PATH)
           ."\nhost: ". parse_url($actorID,PHP_URL_HOST)
           ."\ndate: $date";
	$key = openssl_get_privatekey(file_get_contents("keys/$uuid"));
   // signs $data and puts the signature in $signature
   // returns a 500 error if the function fails for some reason
   if (openssl_sign($data, $signature, $key, OPENSSL_ALGO_SHA256) === FALSE) {
     http_response_code(500);
     die('unable to sign');
   }
 
   // prepare data and signature for use in the headers
   $signature =  base64_encode($signature);
   $keyid =      "keyId=\"$baseurl/ap/actor?$username#main-key\"";
   $algo =       'algorithm="rsa-sha256"';
   $headers =    'headers="(request-target) host date"';
   $signature =  "signature=\"$signature\"";
 
   // define the list of headers
   // Content-Type and Accept must conform to ActivityPub specs
   $h = array(
         'Content-Type: application/activity+json',
         'Accept: application/activity+json,application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
         "Host: ". parse_url($actorID,PHP_URL_HOST),
         "Date: $date",
         "Signature: $keyid,$algo,$headers,$signature"
       );
  // useragent file contains StarShip app attribution and current version number
	$user_agent = trim(file_get_contents('useragent'));
  // post request to obtain public key; later, cache public keys for 24h
  $ch = curl_init($actorID);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $h);
  curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
  $json = curl_exec($ch);
	if (curl_errno($ch) > 0) {
		debug(curl_errno($ch) .':'. curl_error($ch),1);
	}
	debug("response: $json",3);
	curl_close($ch);
  return json_decode($json, true);
}

function update_profile($username, $uuid, &$sql) {
	require_once "activitypub/activities.php";
	global $baseurl;

  try { $newActivity = new StarShip\ProfileUpdate(); $act_success = $newActivity->load($username,$sql); }
  catch(Exception $e) {
    debug($e,1);
    return false;
  }
	if (!$act_success) return false;

	$query = $sql->prepare("SELECT * FROM followers WHERE uuid = ?");
	$query->bind_param('i',$uuid);
	$query->execute();
	if ($query->errno) {
		debug("SQL error: (" . $query->errno . ") " . $query->error,1);
		return false;
	}
	$result = $query->get_result();
	
	if (!($row = $result->fetch_assoc())) {
		debug("no followers found",3);
		return false;
	}

	$sendit = [];
	$sendit['content'] = $newActivity->toJSON();
	$sendit['user'] = $uuid;
	$sendit['actor'] = $baseurl .'/ap/actor?'. $username;
	$sendit['inbox'] = $row['inbox'];

	$bean = json_encode($sendit);
  debug("sending profile update ". $newActivity->id() ." to ". $sendit['inbox'],3);

	$connection  = new Connection('127.0.0.1', 11300);
	$beansClient = new BeansClient($connection);

	$beansClient->useTube('ActivityPets')->put($bean);

	while ($row = $result->fetch_assoc()) {
		$sendit['inbox'] = $row['inbox'];
		debug("sending profile update ". $newActivity->id() ." to ". $sendit['inbox'],3);
		$bean = json_encode($sendit);
		$beansClient->useTube('ActivityPets')->put($bean);
	}
	return true;
}

function post($username,$uuid,$msg,$mentions,$address,&$sql,$delay=false,$inReplyTo=false) {
	require_once "activitypub/activities.php";
	global $baseurl;
	$actor = $baseurl .'/ap/actor?'. $username;

	$posty = [
		          'attributedTo' => $actor,
							'type'         => 'Note',
							'to'           => $address['to'],
							'content'      => $msg,
							'tag'          => []
	         ];
	if ($delay) $posty['published'] = date("Y-m-d\TH:i:s\Z");
	else $posty['published'] = date("Y-m-d\TH:i:s\Z");

	$posty['cc'] = []; $posty['bcc'] = [];
	if (isset($address['cc'])) {
		if (is_array($address['cc']))	$posty['cc'] = $address['cc'];
		else $posty['cc'][] = $address['cc'];
	}
	if (isset($address['bcc'])) {
		if (is_array($address['bcc']))	$posty['bcc'] = $address['bcc'];
		else $posty['bcc'][] = $address['bcc'];
	}
	if ($inReplyTo) $posty['inReplyTo'] = $inReplyTo;
	
	if ($mentions) {
		foreach ($mentions as $mention) {
			$posty['tag'][] = [ 'type' => 'Mention', 'href' => $mention['id'], 'name' => '@'. $mention['name'] ];
			$posty['cc'][] = $mention['id'];
		}
	}
	
  try { $newObject = new StarShip\APObject(); $newObject->fill($posty); }
  catch(Exception $e) {
    debug($e,1);
    return false;
  }

  try { $newActivity = new StarShip\Create(); $newActivity->wrap($newObject); }
  catch(Exception $e) {
    debug($e,1);
    return false;
  }
	$newActivity->save($sql);
	
	$inboxes = [];
	if (is_array($address['to'])) {
		foreach ($address['to'] as $to_id) {
			if ($to_id == $baseurl .'/ap/followers?'. $username) {
				$query = $sql->prepare("SELECT * FROM followers WHERE uuid = ?");
				$query->bind_param('i',$uuid);
				$query->execute();
				if ($query->errno) {
					debug("SQL error: (" . $query->errno . ") " . $query->error,1);
					return false;
				}
				$result = $query->get_result();
				
				if (!($row = $result->fetch_assoc())) debug("no followers found",3);
				else {
					if (array_search($row['inbox'],$inboxes) === false) $inboxes[] = $row['inbox'];
					while ($row = $result->fetch_assoc()) {
						if (array_search($row['inbox'],$inboxes) === false) $inboxes[] = $row['inbox'];
					}
				}
			}
			else {
				// should probably cache inboxes in a table, sigh
				$to_actor = get_actor($to_id,$uuid,$username);
				if (is_array($to_actor)) {
					$inboxes[] = $to_actor['inbox'];
				}
			}
		}
	} else {
		$to_id = $address['to'];
		if ($to_id == $baseurl .'/ap/followers?'. $username) {
			$query = $sql->prepare("SELECT * FROM followers WHERE uuid = ?");
			$query->bind_param('i',$uuid);
			$query->execute();
			if ($query->errno) {
				debug("SQL error: (" . $query->errno . ") " . $query->error,1);
				return false;
			}
			$result = $query->get_result();
				
			if (!($row = $result->fetch_assoc())) debug("no followers found",3);
			else {
				if (array_search($row['inbox'],$inboxes) === false) $inboxes[] = $row['inbox'];
				while ($row = $result->fetch_assoc()) {
					if (array_search($row['inbox'],$inboxes) === false) $inboxes[] = $row['inbox'];
				}
			}
		}
		else {
			// should probably cache inboxes in a table, sigh
			$to_actor = get_actor($to_id,$uuid,$username);
			if (is_array($to_actor)) {
				$inboxes[] = $to_actor['inbox'];
			}
		}
	}
	if ($mentions) {
		foreach ($mentions as $mention) {
			if (array_search($mention['inbox'],$inboxes) === false) $inboxes[] = $mention['inbox'];
		}
	}
	
	$sendit = [];
	$sendit['content'] = $newActivity->toJSON();
	$sendit['user'] = $uuid;
	$sendit['actor'] = $actor;

	$connection  = new Connection('127.0.0.1', 11300);
	$beansClient = new BeansClient($connection);

	foreach ($inboxes as $inbox) {
		$sendit['inbox'] = $inbox;
		debug("sending ". $newActivity->id() ." to ". $sendit['inbox'],3);
		$bean = json_encode($sendit);
		if ($delay)	$beansClient->useTube('StarShip')->put($bean,2048,$delay);
		else $beansClient->useTube('StarShip')->put($bean);
	}
	
	return $newActivity->obj->id($full=false);
}

namespace StarShip {
	class Exception extends \Exception {
	
	}
}
?>
