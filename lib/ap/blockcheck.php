<?php
$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
	return false;
}
$stringcheck = $_SERVER['HTTP_USER_AGENT'];
$query = $sql->prepare("SELECT COUNT(*) AS total FROM blocked_servers WHERE INSTR(?,uri_match) > 0");
$query->bind_param('s',$stringcheck); $query->execute();
if ($query->errno) {
	debug("SQL error: (" . $query->errno . ") " . $query->error,1);
	return false;
}
$result = $query->get_result(); $row = $result->fetch_assoc();
debug($stringcheck .' - '. $row['total'],3);
if ($row['total'] === 0) return true;
else return false;
?>