<?php
namespace StarShip {

/* The base parent class for all ActivityPub Objects */
class APObject {
  
  // define the elements that other stuff should be able to access
  public $attributedTo;
  public $content;
  public $address;
  public $type;
	public $inReplyTo;
  // this is just to track whether or not an object has been initialized with content or is empty
  public $new = true;
  
  // load an activity's properties from the database into the Activity object
  public function load($id, &$sql) {
		global $baseurl;
    if ($this->new === false) throw new Exception('requires a blank AP object');
		$query = $sql->prepare("SELECT * FROM posts JOIN accounts ON accounts.uuid = posts.uuid WHERE id = ?");
		$query->bind_param('s',$id);
		$query->execute();
		if ($query->errno) {
			// log error locally
			file_put_contents("logs/debug.log",$query->errno .': '. $query->error ."\n",FILE_APPEND);
			throw new Exception('database query failed');
		}
		$result = $query->get_result();
		
		if (!($row = $result->fetch_assoc())) {
			return false;
		}
		$attributedTo = $baseurl . '/ap/actor?'. $row['username'];
		$address = json_decode($row['address']);

		$json_array = [
								'id'           => $baseurl .'/ap/post?'. $row['id'],
								'attributedTo' => $attributedTo,
								'type'         => 'Note',
								'to'           => [],
								'cc'           => [],
								'bcc'          => [],
								'published'    => date("Y-m-d\TH:i:s\Z"),
								'content'      => $row['content'],
								'tag'          => json_decode($row['tags'])
						];
		if (is_array($address)) {
			if (is_array($address['to'])) $json_array['to'] = $address['to'];
			else $json_array['to'][] = $address['to'];
			if (is_array($address['cc'])) $json_array['cc'] = $address['cc'];
			else $json_array['cc'][] = $address['cc'];
			if (is_array($address['bcc'])) $json_array['bcc'] = $address['bcc'];
			else $json_array['bcc'][] = $address['bcc'];
		}
		
		$this->fill($json_array);
		return true;
  }

  // Stores the properties of the Activity object into the database
  // use "update on duplicate key"
  public function save(&$sql) {
    if ($this->new === true) throw new Exception('requires a populated Activity object');
		$uuid = self::clip($this->attributedTo);
		if (isset($this->obj_array['tag']))	$tags = json_encode($this->obj_array['tag']);
		else $tags = '';
		$query = $sql->prepare("INSERT INTO posts (id, uuid, content, tags, address, created_at) VALUES (?, (SELECT uuid FROM accounts WHERE username = ?), ?, ?, ?, NOW())");
		$address = json_encode($this->address);
		$query->bind_param("sssss", $this->id, $this->attributedTo, $this->content, $tags, $address);
		$query->execute();
		if ($query->errno) {
			echo "SQL error: (" . $query->errno . ") " . $query->error ."\n"; return false;
		}
		else return true;
  }

  /* loads an activity's properties from an unserialized-JSON array into the Activity object */
  public function fill($json_array) {
		global $baseurl;
    if ($json_array === null) throw new Exception('no AP Object data provided');
    // verify that certain required properties are set
    $valid = self::validateObject($json_array);
    if ($valid === false) throw new Exception('invalid AP Object data');

    // loads the array into the object, isn't this nice
    $this->obj_array = $json_array;
    $this->new = false;

    if (!isset($this->obj_array['id'])) {
			$logid = uniqid('',true);
			$logid = str_replace('.','',$logid);
			$this->id = $logid;
			$this->obj_array['id'] = $baseurl ."/ap/post?". $logid; 
		}
		else {
			$this->id = self::clip($this->obj_array['id']);
		}

    // assign public values to appropriate variables
    $this->attributedTo  = self::clip($this->obj_array['attributedTo']);
    $this->type     = &$this->obj_array['type'];
    $this->content      = &$this->obj_array['content'];
    $this->address  = [];
    if (isset($this->obj_array['to']))  $this->address['to']  = &$this->obj_array['to'];
    if (isset($this->obj_array['cc']))  $this->address['cc']  = &$this->obj_array['cc'];
    if (isset($this->obj_array['bcc'])) $this->address['bcc'] = &$this->obj_array['bcc'];
    
		if (isset($this->obj_array['inReplyTo'])) $this->inReplyTo = &$this->obj_array['inReplyTo'];
		
  }
	
	public function check_tags($uri) {
		if ($this->new == true) return false;
		
		if (isset($this->obj_array['tag'])) {
			foreach ($this->obj_array['tag'] as $tag) {
				if ($tag['href'] == $uri) return true;
			}
		}
		return false;
	}
  
  // this is a function instead of a variable because changing the id is Very Bad
  public function id($full=true) {
    if ($full) return $this->obj_array['id'];
		else return self::clip($this->obj_array['id']);
  }
  public function published() {
    return $this->obj_array['published'];
  }
  
  /* Serializes the Activity object to a ActivityPub JSON-LD object */
  public function toJSON() {
    $json = json_encode($this->obj_array);
    return $json;
  }

  protected $obj_array;
  protected $id;

  /* Check various key properties to make sure that they exist and have valid contents */
  protected function validateObject(&$json_array) {
    if (!isset($json_array['type'])) return false;

    if (!isset($json_array['attributedTo'])) return false;

    return true;
  }

	protected function clip($string) {
		$snip = strpos($string, '?');
		return substr($string, $snip+1);
	}
}

}?>