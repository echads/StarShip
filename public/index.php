<?php session_start(); require_once "../authcheck.php"; ?>
<html>
<head>
<title>StarShip Testing</title>
</head>
<body>
<?php
if ($_SESSION['authed'] !== true) echo '<p>Nothing to see here yet, move along.</p>';

else include "../post.php";
?>
</body>
</html>
