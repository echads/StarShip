<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once 'config.php';
$allowed = include_once 'lib/ap/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}
header('Content-Type: application/json; charset=UTF-8');
?>
{"links":[{"rel":"http://nodeinfo.diaspora.software/ns/schema/2.0","href":"<?php echo $baseurl; ?>/api/nodeinfo?2.0"}]}