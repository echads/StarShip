<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require_once '../config.php';

$error = "I said FINGERPRINTS!";

if (isset($_GET['resource'])) {
	$account = $_GET['resource'];

	$snip = strpos($account, ':');
	$snop = strpos($account, '@');
	$name = substr($account, $snip+1, $snop-$snip-1);
	$host = substr($account, $snop+1);
	
	if ($baseurl != ('https://'. $host)) {
		echo $error; return;
	}

	$sql_success = include_once '../dbconnect.php';
	if ($sql_success !== true) { 
		http_response_code(500);
		die();
	}

	$query = $sql->prepare("SELECT username FROM accounts WHERE username = ?");
	$query->bind_param('s',$name);
	$query->execute();
  if ($query->errno) { http_response_code(500); die(); }
  $result = $query->get_result();
  if ($row = $result->fetch_assoc()) {
		header('Content-Type: application/json');
		echo '{"subject":"'. $account .'",';
		echo '"links":[{"rel":"self","type":"application/activity+json",';
		echo '"href":"'. $baseurl .'/ap/actor?'. $row['username'] .'"}]}';
	}
	else echo $error;
}

else echo $error;

?>
