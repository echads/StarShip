<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once 'config.php';
require_once 'lib/meta.php';

$allowed = include_once 'lib/ap/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}

$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
  http_response_code(500);
  die('database connection failed');
}

$GET_keys = array_keys($_GET);
$username = $GET_keys[0];

$query = $sql->prepare("SELECT * FROM accounts WHERE username = ?");
$query->bind_param('s',$username);
$query->execute();
if ($query->errno) {
	// log error locally
	debug($query->errno .': '. $query->error,1);
	// send 500 error
  http_response_code(500);
  die('database query failed');
}
$result = $query->get_result();

if (!($row = $result->fetch_assoc())) {
	// return a 404
  http_response_code(404);
  die();
}
$actorID = $baseurl . '/api/actor?'. $username;

$jarr = [];
$jarr['@context'] = [	"https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1"	];
$jarr['id'] = $actorID;
$jarr['type'] = 'Service';
$jarr['preferredUsername'] = $row['username'];
$jarr['name'] = $row['displayname'];
$jarr['icon'] = [	"type" => "Image", "mediaType" => "image/png", "url" => $baseurl . '/i?'. $row['username'] ];
$jarr['summary'] = $row['bio'];
$jarr['inbox'] = $baseurl . '/api/inbox?'. $row['username'];
$jarr['outbox'] = $baseurl . '/api/outbox?'. $row['username'];
$jarr['followers'] = $baseurl .'/api/followers?'. $row['username'];
$jarr['url'] = $baseurl . '/profile?'. $row['username'];
$jarr['publicKey'] = [ 'id' => $actorID .'#main-key', 'owner' => $actorID, 'publicKeyPem' => $row['pubkey'] ];

$jarr['attachment'] = json_decode($row['attachment']);

header('Content-Type: application/activity+json; charset=utf-8');
echo json_encode($jarr);
?>