<?php

chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once "config.php";
require_once "lib/meta.php";
require_once "lib/ap/utils.php";
require_once "lib/ap/activities.php";
require_once "vendor/autoload.php";

use xobotyi\beansclient\BeansClient;
use xobotyi\beansclient\Connection;

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	http_response_code(405);
	die('Request method not permitted');
}

$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
  http_response_code(500);
  die('database connection failed');
}
$allowed = include_once 'activitypub/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}

$GET_keys = array_keys($_GET);
$username = strtolower($GET_keys[0]);
$headers = getRequestHeaders();

$query = $sql->prepare("SELECT * FROM accounts WHERE username = ?");
$query->bind_param('s',$username);
$query->execute();
if ($query->errno) {
	// log error locally
	debug($query->errno .': '. $query->error,1);
	// send 500 error
  http_response_code(500); die('database query failed');
}
$result = $query->get_result();

if (!($row = $result->fetch_assoc())) {
	// return a 404
	debug("account not found",3);
  http_response_code(404); die('page not found');
}

$username = $row['username']; $uuid = $row['uuid'];

// parse json
$post_content = json_decode(file_get_contents("php://input"),true);
debug("Activity by: ". $post_content['actor'],3);
$newActivity = null;

$actor_data = get_actor($post_content['actor'],$uuid,$username);

if ($post_content['type'] == 'Delete') {
	debug("Delete Activity",3);
	return;
}

$verified = verify_signature($headers, $actor_data, $username);
// return Unauthorized with informative message if signature is wrong
if ($verified === false) {
	debug("invalid signature: ". $verified,3);
  http_response_code(403);
  die('Invalid signature.');
}
// return Server Error with informative message if signature verification outright fails
if ($verified !== true) {
	debug("signature verification failed". $verified,2);
  http_response_code(500);
  die('Error verifying signature: '. $verified);
}
// signature is verified, continue

debug("POST to inbox, signature verified",3);

if ($post_content['type'] == 'Announce') {
	debug("Announce Activity",3);
	return;
}

$printout = '';
$response = [];
// currently only has special-case handling for Create and Follow Activities
// goal: expand this to all supported types, then write sufficiently flexible default case to handle all others
switch($post_content['type']) {
  // process a Create activity
  case "Create":
    try { $newActivity = new StarShip\Create(); $newActivity->fill($post_content); }
    catch(Starship\Exception $e) {
      debug($e,1);
      break;
    }
    debug("Created a ". $newActivity->obj->type,3);
    debug($newActivity->obj->content,3);
		// this needs to save the incoming posts
    break;
  // process a Follow activity
  case "Follow":
    try { $newActivity = new StarShip\Follow(); $newActivity->fill($post_content); }
    catch(Starship\Exception $e) {
      debug($e,1);
      break;
    }
    debug("Sent ". $newActivity->type ." ". $newActivity->id(),3);
		// this needs to actually handle storing requests to be handled by the end user
    break;
	case "Announce":
		$printout = "Sent ". $newActivity->type ." ". $newActivity->id() ."\n";
		return; break;
  default:
    try { $newActivity = new StarShip\Activity(); $newActivity->fill($post_content); }
    catch(Starship\Exception $e) {
      debug($e,1);
      break;
    }
    debug($post_content['type'] .' '. $post_content['id'],3);
}


return;
?>
