<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once 'config.php';
require_once 'lib/meta.php';
require_once 'lib/ap/activities.php';

$allowed = include_once 'lib/ap/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}

$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
  http_response_code(500);
  die('database connection failed');
}

$GET_keys = array_keys($_GET);
$logID = $GET_keys[0];

try { $activity = new StarShip\Activity(); $success = $activity->load($logID, $sql); }
catch(Exception $e) {
  debug($e,2);
  http_response_code(500);
  die($e);
}

if (!$success) {
	// return a 404
  http_response_code(404);
  die();
}

header('Content-Type: application/activity+json; charset=UTF-8');

echo $activity->toJSON();
?>