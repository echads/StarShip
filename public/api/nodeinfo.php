<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once 'config.php';
require_once 'lib/meta.php';

$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
  http_response_code(500);
  die('database connection failed');
}

$GET_keys = array_keys($_GET);
if (!is_array($GET_keys) || empty($GET_keys)) {
  http_response_code(404);
  die();
}
	
$version = $GET_keys[0];

if ($version != '2_0') {
  http_response_code(404);
  die();
}
$charcount = 0; $logcount = 0;
$char_query = $sql->prepare("SELECT COUNT(*) AS total FROM characters");
$char_query->execute();
if ($char_query->errno) {
  debug("SQL error: (" . $char_query->errno . ") " . $char_query->error,2);
} else {
	$result = $char_query->get_result(); $row = $result->fetch_assoc();
	$charcount = $row['total'];
}
$log_query = $sql->prepare("SELECT COUNT(*) AS total FROM logs");
$log_query->execute();
if ($log_query->errno) {
	debug("SQL error: (" . $log_query->errno . ") " . $log_query->error,2);
} else {
	$result = $log_query->get_result(); $row = $result->fetch_assoc();
	$logcount = $row['total'];
}

header('Content-Type: application/activity+json; charset=UTF-8');
?>

{
	"version":"2.0",
	"software":{
		"name":"starship",
		"version":"<?php echo trim(file_get_contents('version')); ?>"
	},
	"protocols":[
		"activitypub"
	],
	"usage":{
		"users":{
			"total":<?php echo $charcount; ?>,
			"activeMonth":0,
			"activeHalfyear":0
		},
		"localPosts":<?php echo $logcount; ?>
	},
	"openRegistrations":false
}

