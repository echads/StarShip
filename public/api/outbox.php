<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');

//temporary
http_response_code(403);
die();

$allowed = include_once 'lib/ap/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}

require_once "lib/ap/utils.php";
require_once "lib/ap/activities.php";


$headers = getRequestHeaders();
if ($headers['Request-Method'] !== 'GET') {
	http_response_code(405);
	die('Request method not permitted');
}

$verified = verify_signature($headers);
// return Unauthorized with informative message if signature is wrong
if ($verified === false) {
  http_response_code(403);
  die('Invalid signature.');
}
// return Server Error with informative message if signature verification outright fails
if ($verified !== true) {
  http_response_code(500);
  die('Error verifying signature: '. $verified);
}
// signature is verified, continue

$authorized = false;
// check if actor is authorized to get from this outbox
// value goes into $authorized
if ($authorized === false) {
  http_response_code(403);
  die();
}

// the outbox should return an OrderedCollection of an accounts posts IF and ONLY if the requester is authenticated as the bot's owner-partner, otherwise it should return an empty collection

// i should probably make the OrderedCollection generation in the StarShip library