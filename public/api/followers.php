<?php
chdir($_SERVER['DOCUMENT_ROOT']);
chdir('../');
require_once 'config.php';
require_once 'lib/meta.php';

$allowed = include_once 'lib/ap/blockcheck.php';
if ($allowed !== true) { 
  http_response_code(403);
  die();
}

$sql_success = include_once 'dbconnect.php';
if ($sql_success !== true) { 
  http_response_code(500);
  die('database connection failed');
}

$GET_keys = array_keys($_GET);
$username = $GET_keys[0];

if ($username == $manager)  $query = $sql->prepare("SELECT * FROM accounts WHERE username = ?");
else $query = $sql->prepare("SELECT * FROM accounts JOIN characters ON accounts.uuid = characters.uuid WHERE username = ?");
$query->bind_param('s',$username);
$query->execute();
if ($query->errno) {
	// log error locally
	debug($query->errno .': '. $query->error,1);
	// send 500 error
  http_response_code(500);
  die('database query failed');
}
$result = $query->get_result();

if (!($account = $result->fetch_assoc())) {
	// return a 404
  http_response_code(404);
  die();
}

$query = $sql->prepare("SELECT actor FROM followers WHERE uuid = ?");
$query->bind_param('i',$account['uuid']);
$query->execute();
if ($query->errno) {
	// log error locally
	debug($query->errno .': '. $query->error,1);
	// send 500 error
  http_response_code(500);
  die('database query failed');
}
$result = $query->get_result();
$followers = [];
while ($row = $result->fetch_assoc()) {
	$followers[] = $row['actor'];
}

header('Content-Type: application/activity+json; charset=UTF-8');

?>
{
	"@context":[
		"https://www.w3.org/ns/activitystreams",
		"https://w3id.org/security/v1"
	],
	"id":"<?php echo $baseurl .'/ap/followers?'. $account['username']; ?>",
	"type":"Collection",
	"totalItems":"<?php echo count($followers); ?>",
	"items":<?php echo json_encode($followers); ?>
}