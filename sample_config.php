<?php
$sitetitle  = "StarShip";
$sitedesc   = "A federated social media server";
$baseurl    = "http://example.com";
$sitebg     = "#EEEEEE";
$sitecolor  = "#DDDDDD";
$linkcolor  = "#888888";

$db_name     = "StarShip";
$db_account  = "StarShip";
$db_password = "your password";	// replace with ident authentication once i figure it out

$debug_level = 0; // 0 for "off", 1 for errors, 2 for warnings, 3 for info

?>