-- base account stuff
CREATE TABLE accounts (
  uuid bigint unsigned not null primary key,
  actor text not null,
	username varchar(32) not null,
	name varchar(64) not null,
  email text,
	summary text,
	attachment text,
	privkey text not null,
  pubkey text not null,
  hash char(255)
);

CREATE TABLE remote_accounts (
  actor text not null primary key,
  account_name varchar(64) not null,
  name varchar(64),                         -- display name
  pubkey text not null,
  inbox text not null
);

-- all active/approved Followers for local accounts
CREATE TABLE followers (
  uuid bigint unsigned not null,         -- local account's UUID
  actor text not null,                   -- remote-or-local follower actor ID
  local boolean default false,           -- whether the actor is local, for convenience
  PRIMARY KEY (uuid,actor)
);

-- all accounts that local accounts are Following
CREATE TABLE follows (
  uuid bigint unsigned not null,         -- local account's UUID
  actor text not null,                   -- remote-or-local follower actor ID
  local boolean default false,           -- whether the actor is local, for convenience
  PRIMARY KEY (uuid,actor)
);

-- groups of accounts that a local account has put together
CREATE TABLE groups {
	uuid char(24) not null,
	group_id bigint not null,
	group_name varchar(32) not null,
	PRIMARY KEY (uuid,group_id)
};

-- accounts that have been put into groups
CREATE TABLE grouped {
	group_id bigint not null,
  actor text not null,                 -- remote-or-local followed actor ID
	PRIMARY KEY (uuid,group_id)
};

CREATE TABLE activities (
  id char(24) primary key,
  actor varchar(32) not null,
  activity_type varchar(16) not null,
	obj_id text,
	obj_str text,
  created_at datetime not null,
	to_addr text,
	cc_addr text,
	bcc_addr text
);

CREATE TABLE posts (
  id char(24) primary key,
  uuid bigint unsigned not null,
  content text not null,
	tags text,
	address text not null,
  created_at datetime not null
);

/*
CREATE TABLE activities (
  id varchar() not null primary key,
  actor varchar() not null,
	addressing text not null,
  recieved date not null,
  updated date,
  activity_type int not null,               -- change this to use an ENUM
	obj_type int,                             -- this too
	obj_id varchar() not null
);

CREATE TABLE objects (
  id varchar() not null primary key,
  attribution varchar() not null,
  obj_type int not null,                    -- change to use ENUM
  published date not null,
  updated date,
	json text not null
);
*/